import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.5


Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Rectangle{
        id : rect
        width: 300
        height: 200
        x : 200
        Text {
            id: txt
            focus: true
            text: txt.focus + " " + txt.activeFocus
        }
    }

    Rectangle{
        id : rect2
        width: 300
        height: 200
        x : 400
        Text {
            id: txt2
            focus: true
            text: txt2.focus + " " + txt2.activeFocus
        }
    }


    Button{
        id : btn
        width: 100
        height: 50
        focus: false
        background: Rectangle{
            anchors.fill: parent
            border.color: btn.focus?"blue":"red"
        }
        onFocusChanged: {
            console.log("focus :", focus)
        }

        Component.onCompleted: {
            console.log("focusPolicy : ", focusPolicy)
        }
        onFocusReasonChanged: {
            console.log("focusReason : ", focusReason)
        }
    }


}
