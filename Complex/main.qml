import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.4

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    ListView{
        id : list2
        x : 450
        width: 100
        height: 300
        model: ["aa", "bb", "cc"]
        delegate: Column {
            Text {
                //id: name
                text: modelData + "  text"
            }
            Button{
                text: modelData + "  btn"
            }
        }
    }



    ListView{
        id : list
        x : 350
        width: 100
        height: 300
        model: ["aa", "bb", "cc"]
        delegate: Text {
            id: view
            text: modelData
        }
    }


    Repeater{
        id : rep
        model : 3
        Button{
            //id : btn
            y : index * 60
            text : "btn" + index
        }
    }

    Column{
        x : 200
        id : col
        Button{
            id : btn
            text: "btn"
        }
        Text {
            id: txt
            text: qsTr("text")
        }
        Rectangle{
            id : rect
            width: 100
            height: 20
            color: "red"
        }
    }

    Button{
        anchors.bottom: parent.bottom
        onClicked: {
            for(var i=0; i<rep.count; i++){
                console.log(rep.itemAt(i).text)
            }
            console.log(btn.text+"hello")
            for(i=0; i<col.children.length; i++){
                console.log(col.children[i] instanceof Button)
            }

            for(i=0; i<list.contentItem.children.length; i++){
                //console.log(list.contentItem.children[i])
                if(list.contentItem.children[i] instanceof Text){
                    console.log(list.contentItem.children[i].text)
                }
            }

            for(i=0; i<list2.contentItem.children.length; i++){
                var col1 = list2.contentItem.children[i]
                //console.log(col1)
                if(col1 instanceof Column){
                    for(var j=0; j<col1.children.length; j++){
                        console.log(col1.children[j].text)
                    }
                }
            }

        }
    }
}
