import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 1.4

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")



    Component {
        id: com

        Rectangle {
            id : rect
              color: "red"
              width: 100
              height: 100

              Component.onCompleted: {
                  console.log("onCompleted:", width, height, color)
              }

              Component.onDestruction: {
                  console.log("onDestruction")
              }
        }
    }

    Button{
        width: 100
        height: 100
        x : 200
        onClicked: {
            console.log("click")
            loader.item.width = 50
            loader.item.color = "green"
            //loader.sourceComponent = null
        }
    }

    Loader {
        id : loader
        sourceComponent: com
        asynchronous: true
        onStatusChanged: {
            console.log("status", status)
        }
    }


}
