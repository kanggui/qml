import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Rectangle {
        x : 0
        y : 300
          color: "yellow"
          width: 100; height: 100

          MouseArea {
              anchors.fill: parent
              onClicked: console.log("clicked yellow")
          }

          Rectangle {
              color: "blue"
              width: 50; height: 50

              MouseArea {
                  anchors.fill: parent
                  propagateComposedEvents: true
                  onClicked: {
                      console.log("clicked blue")
                      //mouse.accepted = false
                  }
              }
          }
      }

    Rectangle {
        x : 200
        y : 0
        width: 480
        height: 320
        Rectangle {
            x: 30; y: 30
            width: 300; height: 240
            color: "lightsteelblue"

            MouseArea {
                anchors.fill: parent
                drag.target: parent;
                drag.axis: "XAxis"
                drag.minimumX: 30
                drag.maximumX: 150
                drag.filterChildren: false

                Rectangle {
                    color: "yellow"
                    x: 50; y : 50
                    width: 100; height: 100
                    MouseArea {
                        anchors.fill: parent
                        onClicked: console.log("Clicked")
                    }
                }
            }
        }
    }


    Rectangle {
        x : 200
        y : 200
          id: container
          width: 600; height: 200

          Rectangle {
              id: rect
              width: 50; height: 50
              color: "red"
              opacity: (600.0 - rect.x) / 600

              MouseArea {
                  anchors.fill: parent
                  drag.target: rect
                  drag.axis: Drag.XAxis | Drag.YAxis
                  drag.minimumX: 0
                  drag.maximumX: container.width - rect.width
              }
          }
      }

    MouseArea{
        id : mouseArea
        width : 100
        height : 100
        Rectangle{
            anchors.fill: parent
            color: "red"
        }
        onClicked: {
            console.log("clicked")
        }
        //hoverEnabled: true
        onContainsMouseChanged: {
            console.log("onContainsMouseChanged", containsMouse)
        }
        onContainsPressChanged: {
            console.log("onContainsPressChanged", containsPress)
        }

        onPressAndHold: {
            console.log("onPressAndHold")
        }

//        onPressed: {
//            var ret  = pressedButtons & Qt.LeftButton
//            var ret1 = pressedButtons & Qt.RightButton
//            console.log(ret?"left": ret1?"right":"other")
//
//            console.log("onPressed")
//        }
//        onReleased: {
//            console.log("onReleased")
//        }
        acceptedButtons: Qt.LeftButton | Qt.RightButton
    }
}
