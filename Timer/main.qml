import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.4

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Button{
        width: 100
        height: 100
        anchors.bottom:  parent.bottom
        onClicked: {
            timer.start()
        }
    }

    Item {
          Timer {
              id : timer
              interval: 2000
              //running: true
              triggeredOnStart: true
              repeat: true
              onTriggered: {
                  time.text = Date().toString()
              }
          }

          Text {
              id: time
              font.pixelSize: 35
          }
      }


}
