import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Component {
          id: sectionHeading
          Rectangle {
              width: 400
              height: childrenRect.height
              color: "lightsteelblue"

              Text {
                  text: section
                  font.bold: true
                  font.pixelSize: 20
              }
          }
      }

    ListView {
        x : 200
          width: 180; height: 200

          contentWidth: 320

          verticalLayoutDirection :Qt.LeftToRight
          //orientation : ListView.Horizontal
          flickableDirection: Flickable.AutoFlickIfNeeded

          model: model_id
          delegate: Row {
              Text { text: '<b>Name:</b> ' + name; width: 160 }
              Text { text: '<b>Number:</b> ' + number; width: 160 }
          }
          header: Rectangle{
              width: 400
              height: 20
              color: "green"
          }
          footer: Rectangle{
              width: 400
              height: 20
              color: "blue"
          }

          spacing: 20
          highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
          focus: true

          section.property: "size"
          section.criteria: ViewSection.FullString
          section.delegate: sectionHeading
      }

    ListModel {
        id : model_id
          ListElement {
              name: "Bill Smith"
              number: "555 3264"
              size : "midium"
          }
          ListElement {
              name: "John Brown"
              number: "555 8426"
              size : "small"
          }
          ListElement {
              name: "Sam Wise"
              number: "555 0473"
              size : "large"
          }

          ListElement {
              name: "aaa Smith"
              number: "111 3264"
              size : "midium"
          }
      }

/*
    ListView {
          width: 180; height: 200

          Component {
              id: contactsDelegate
              Rectangle {
                  id: wrapper
                  width: 180
                  height: contactInfo.height
                  color: ListView.isCurrentItem ? "black" : "red"
                  Text {
                      id: contactInfo
                      text: name + ": " + number
                      color: wrapper.ListView.isCurrentItem ? "red" : "black"
                  }
              }
          }

          model: model_id
          delegate: contactsDelegate
          focus: true
      }
*/
}
