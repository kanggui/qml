#include "mymodel.h"

MyModel::MyModel(QObject *parent)
    : QAbstractListModel(parent)
{
    list.append(MyData("aa", 111));
    list.append(MyData("bb", 222));
}
/*
QVariant MyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    // FIXME: Implement me!
}*/

int MyModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    return list.count();
    // FIXME: Implement me!
}

QVariant MyModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    // FIXME: Implement me!
    if(role == Name)
    {
        return list[index.row()].name;
    }
    else if(role == Value)
        return list[index.row()].value;
    return QVariant();
}

QHash<int, QByteArray> MyModel::roleNames() const
{
    QHash<int, QByteArray> role;
    role.insert(Name, "name");
    role.insert(Value, "value");

    return role;
}
