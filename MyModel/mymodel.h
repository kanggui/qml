#ifndef MYMODEL_H
#define MYMODEL_H

#include <QAbstractListModel>
#include <QHash>

class MyData
{
public:
    MyData(QString name, int v):name(name),value(v)
    {}
    QString name;
    int value;
};

class MyModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum MyRole{
        Name = Qt::DisplayRole + 1,
        Value,
    };
    static MyModel *Get()
    {
        static MyModel m;
        return &m;
    }

    explicit MyModel(QObject *parent = nullptr);

    // Header:
    //QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
    QHash<int, QByteArray> roleNames()const override;
    QList<MyData> list;

};

#endif // MYMODEL_H
