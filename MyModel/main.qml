import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    ListView{
        width: 200
        height: 300
        model: MyModel
        delegate: Text {
            id: txt
            text: name + " " + value
        }
    }
}
