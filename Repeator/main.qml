import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.5

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Row {
        Repeater {
            //model: 3
            model:["aaa", "bbbb", "ccc"]
            Button {
                width: 100; height: 40
                text : modelData
            }
        }
    }
}
