import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.14
import QtQuick.Controls 2.4

Window {
    visible: true
    width: 480
    height: 360
    title: qsTr("Hello World")
    id : root

    property int duration: 3000

    Rectangle {
        id: sky
        width: parent.width
        height: 200
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#0080FF" }
            GradientStop { position: 1.0; color: "#66CCFF" }
        }
    }
    Rectangle {
        id: ground
        anchors.top: sky.bottom
        //anchors.bottom: root.bottom
        height: 160
        width: parent.width
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#00FF00" }
            GradientStop { position: 1.0; color: "#00803F" }
        }
    }

    Rectangle {
        id: ball
        x: 20; y: 240
        width: 50
        height: 50
        color: "red"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                ball.x = 20; ball.y = 240
                anim.restart()
            }
        }
    }

    ParallelAnimation {
        running: true
        id: anim
        SequentialAnimation {
            NumberAnimation {
                target: ball
                properties: "y"
                to: 20
                duration: root.duration * 0.4
                easing.type: Easing.OutCirc
            }
            NumberAnimation {
                target: ball
                properties: "y"
                to: 240
                duration: root.duration * 0.6
                easing.type: Easing.OutBounce
            }
        }
        NumberAnimation { // X1 animation
            target: ball
            properties: "x"
            to: 400
            duration: root.duration
        }

        RotationAnimation {
            target: ball
            properties: "rotation"
            to: 720
            duration: root.duration*1.1
        }
    }
/*
    SequentialAnimation {
        running: true
        id: anim
        NumberAnimation {
            target: ball
            properties: "y"
            to: 20
            // 60% of time to travel up
            duration: root.duration*0.4
        }
        NumberAnimation {
            target: ball
            properties: "x"
            to: 240
            // 40% of time to travel sideways
            duration: root.duration*0.6
        }
    }*/
}
