#include "myobject.h"

MyObject::MyObject(QObject *parent) : QObject(parent)
{

}

void MyObject::setValue(const QString &_context)
{
    name = _context;
}

QString MyObject::getValue()
{
    return name;
}

void MyObject::func()
{
    qDebug() << __FUNCTION__;
    emit mySignal();
}

void MyObject::test(QString name)
{
    qDebug() << name;
}
