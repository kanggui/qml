#ifndef MYOBJECT_H
#define MYOBJECT_H

#include <QObject>
#include <QtQml>

class MyObject : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ getValue WRITE setValue NOTIFY contextChange)
public:

    static MyObject *Get()
    {
        static MyObject obj;
        return &obj;
    }

    explicit MyObject(QObject *parent = nullptr);

    void setValue(const QString &);
    QString getValue();
    Q_INVOKABLE void func();
private:
    QString name;
signals:
    void contextChange(const QString &);
    void mySignal();

public slots:
    void test(QString name);
};

#endif // MYOBJECT_H
