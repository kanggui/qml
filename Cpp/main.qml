import QtQuick 2.9
import QtQuick.Window 2.2
import MyObj 1.0
import QtQuick.Controls 2.4

Window {
    objectName: "root"
    id : root
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    signal sigTest(string name)
    function func(){
        //obj.test(data)
        console.log(data)
        console.log("111")
    }

    function test(data){
        console.log(data)
    }

    MyObject{
        id : obj
        name : "hello"
        Component.onCompleted: {
            //console.log(name)
        }
    }

    Button{
        objectName: "btn"
        width: 100
        height: 100
        onClicked: {
            //sigTest("hello")
            obj.func()
        }
    }

    Component.onCompleted: {
        //sigTest.connect(obj.test)
        //obj.mySignal.connect(func);
    }

//    Connections{
//        target: root
//        onSigTest{
//
//        }
//        //function onSigTest(name){
//        //    obj.test(name)
//        //}
//    }
}
