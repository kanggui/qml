#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtQml>

#include "myobject.h"


int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qmlRegisterType<MyObject>("MyObj", 1, 0, "MyObject");


    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    QList<QObject*> list = engine.rootObjects();
    QObject *obj = list.first();
    if(obj)
    {
        qDebug() << obj->objectName();
        QObject::connect(obj, SIGNAL(sigTest(QString)), MyObject::Get(), SLOT(test(QString)));
        QObject::connect(MyObject::Get(), SIGNAL(mySignal()), obj, SLOT(func()));

        MyObject::Get()->mySignal();

        QVariant res;
        QVariant arg = "123";
        QMetaObject::invokeMethod(obj, "test", Q_RETURN_ARG(QVariant,res),
                                  Q_ARG(QVariant, arg));
    }
    else
        qDebug() << "11";


    //qmlRegisterSingletonInstance("MyObj", 1,0,"MyObject", MyObject::Get());

    return app.exec();
}
