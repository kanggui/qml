import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.4
import QtQuick 2.12
import QtGraphicalEffects 1.12

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")
    id : root

    Grid{
        id : grid
        x : 300
        y : 200
        width: 15
        height: 200
        columns: 3
        Repeater {
            model: grid.width/5 * grid.height/5
                Rectangle{
                width: 5
                height: 5
                color: index%2?"black":"white"
                Component.onCompleted: {
                    console.log(index)
                }
            }
        }
    }

    Rectangle{
        id : rect
        width:grid.width
        height: grid.height
        radius: 10
        border.color: "black"
    }


    Rectangle{
        x : 400
        y : 100
        width: grid.width + 4
        height: grid.height + 4
        border.color: "black"
        radius: 10
        border.width: 3

        OpacityMask{
            source: grid
            maskSource: rect
            anchors.fill: parent
            anchors.margins: 2
        }
    }





    Grid {
        y : 100
      columns: 3
      spacing: 2
      Rectangle { color: "red"; width: 50; height: 50 }
      Rectangle { color: "green"; width: 20; height: 50 }
      Rectangle { color: "blue"; width: 50; height: 20 }
      Rectangle { color: "cyan"; width: 50; height: 50 }
      Rectangle { color: "magenta"; width: 10; height: 10 }
  }

    Flow {
      //x : 100
      anchors.fill: parent
      anchors.margins: 4
      spacing: 10

      Text { text: "Text"; font.pixelSize: 40 }
      Text { text: "items"; font.pixelSize: 40 }
      Text { text: "flowing"; font.pixelSize: 40 }
      Text { text: "inside"; font.pixelSize: 40 }
      Text { text: "a"; font.pixelSize: 40 }
      Text { text: "Flow"; font.pixelSize: 40 }
      Text { text: "item"; font.pixelSize: 40 }
  }

    Column{
        id : col
        spacing: 30
        leftPadding: 50
        Repeater{
            id : rep
            model: ListModel{

            }

            Button{
                width: 100
                height: 100
                text : index
            }
        }
       move: Transition {
             NumberAnimation { properties: "x,y"; duration: 1000 }
         }

       add: Transition {
            NumberAnimation { properties: "x,y"; easing.type: Easing.OutBounce }
        }

        populate: Transition {
              NumberAnimation {
                  properties: "x,y";
                  from: 200;
                  duration: 3000;
                  easing.type: Easing.OutBounce
              }
          }

    }

    Button{
        width: 100
        height: 100
        anchors.bottom: parent.bottom
        onClicked: {
            rep.model.insert(0, {"name":rep.model.count})

            grid.height -= 10
        }
    }

}
