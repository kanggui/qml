import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.5

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Popup {
        x : 500
          id: pop
          width: 100
          height: 100
          modal: true
          visible: true

          Overlay.modal: Rectangle {
              color: "#aacfdbe7"
          }
      }

    Button{
        width: 100
        height : 100
        onClicked: {
            popup.close()
        }
    }

    Popup{
        x : 200
        id : popup
        width: 200
        height: 300
        visible: true
    }
}
