import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")


    Rectangle{
        id : rect
        x : 100
        y : 0
        width: 200
        height: 200
        color: "red"
        focus: true//使用key时必要

        border.width: 5
        border.color: "green"
        antialiasing: false
        radius: 20
        MouseArea{
            anchors.fill: parent
            onClicked:
                console.log("press")
        }

        Keys.onReturnPressed:
            console.log("return key press")
    }

    Rectangle{
        id : rect1
        width: 200
        height: 200
        color: "red"
        anchors.left:  rect.right
        anchors.leftMargin: 50
        rotation: 45
    }

    Rectangle{
        id : rect3
        width: 200
        height: 200
        color: "red"
        anchors.top:  rect.bottom
        anchors.topMargin: 50
        scale: 2
    }

    Rectangle {
            anchors.left: rect3.right
            anchors.leftMargin: 50
            width: 80; height: 80
            gradient: Gradient {
              GradientStop { position: 0.0; color: "lightsteelblue" }
              GradientStop { position: 1.0; color: "blue" }
        }
    }
}
