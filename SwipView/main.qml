import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.5
import QtQml.Models 2.12
import QtQuick.XmlListModel 2.0

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    XmlListModel
    {
        id: xmlModel
        source: "1.xml"
        query: "/rss/channel/item"

        XmlRole {
            name: "title";
            query:  "title/string()"
        }
        XmlRole {
            name: "pubDate";
            query: "pubDate/string()"
        }
        XmlRole {
            name: "link";
            query: "link/string()"
        }
    }

    ListView
    {
        x : 300
        width: 200
        height: 300
       // anchors.fill: parent
        model: xmlModel
        delegate: Text {
            text: title + ": " + pubDate + "; link: " + link
        }
    }


    Rectangle{
        width: 200
        height: 200

        SwipeView {
              id: view
              clip: true

              currentIndex: 0

              anchors.fill: parent

              Item {
                  id: firstPage
                  Rectangle{
                      anchors.fill: parent
                      color: "red"
                  }
              }
              Item {
                  id: secondPage
                  Rectangle{
                      anchors.fill: parent
                      color: "green"
                  }
              }
              Item {
                  id: thirdPage
                  Rectangle{
                      anchors.fill: parent
                      color: "blue"
                  }
              }
          }

          PageIndicator {
              id: indicator

              count: view.count
              currentIndex: view.currentIndex

              anchors.bottom: view.bottom
              anchors.horizontalCenter: parent.horizontalCenter
          }
    }

    Button{
        id : btn
        width: 200
        height: 200
        anchors.bottom: parent.bottom

    }

    ObjectModel{
        id : myModel
        Rectangle{
            width: 200
            height: 200
            color: "red"
        }
        Rectangle{
            width: 200
            height: 200
            color: "green"
        }
        Rectangle{
            width: 200
            height: 200
            color: "blue"
        }
    }

    ListView{
        anchors.left: btn.right
        anchors.bottom: parent.bottom
        height: 300
        width: 200
        model: myModel
        orientation: ListView.Horizontal
        snapMode: ListView.SnapOneItem
        clip: true
    }
}
