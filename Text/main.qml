import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Text {
        x:300
        y:50
        textFormat: Text.RichText
        text: "See the <a href=\"http://qt-project.org\">Qt Project website</a>."
        onLinkActivated:
            console.log(link + " link activated")
        MouseArea{
            anchors.fill: parent
            hoverEnabled: true
            cursorShape: Qt.PointingHandCursor
        }
    }

    Column {
        y:300
          Text {
              font.pointSize: 24
              text: "<b>Hello</b> <i>World!</i>"
          }
          Text {
              font.pointSize: 24
              textFormat: Text.RichText
              text: "<b>Hello</b> <i>World!</i>"
          }
          Text {
              font.pointSize: 24
              textFormat: Text.PlainText
              text: "<b>Hello</b> <i>World!</i>"
          }
      }



    Row {
          Text { font.pointSize: 24; text: "Normal" }
          Text { font.pointSize: 24; text: "Raised"; style: Text.Raised; styleColor: "#AAAAAA" }
          Text { font.pointSize: 24; text: "Outline";style: Text.Outline; styleColor: "red" }
          Text { font.pointSize: 24; text: "Sunken"; style: Text.Sunken; styleColor: "#AAAAAA" }
      }

    Rectangle{
        y : 100
        width: 50
        height: 200
        border.color: "black"
        Text {
            anchors.fill: parent
            id: name
            text: qsTr("text 45641 615 6165")
            wrapMode: Text.WordWrap
            clip: Text.ElideRight
            //font.letterSpacing: 10
            font.underline: true

            Component.onCompleted: {
                console.log(contentWidth)
                console.log(contentHeight)
            }
        }
    }


}
