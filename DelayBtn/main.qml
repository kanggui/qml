import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Extras 1.4
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Button{
        x : 400
        id : control
        text: "button"
        contentItem: Text {
            anchors.fill: parent
            text: control.text
            font.bold: true
            //horizontalAlignment: Text.horizontalCenter
            //verticalAlignment: Text.verticalCenter
        }
    }

    ToolBar {
        x : 300
        y : 200
              RowLayout {
                  anchors.fill: parent
                  ToolButton {
                      text: qsTr("‹")
                      onClicked: stack.pop()
                  }
                  Label {
                      text: "Title"
                      elide: Label.ElideRight
                      horizontalAlignment: Qt.AlignHCenter
                      verticalAlignment: Qt.AlignVCenter
                      Layout.fillWidth: true
                  }
                  ToolButton {
                      text: qsTr("⋮")
                      onClicked: menu.open()
                  }
              }
          }

    RoundButton{
        text : "test"
        x : 200
        y : 350
        width: 100
        height: 100
        radius: 10
    }

    TabBar {
        id: bar
        width: parent.width
        y : 300
        TabButton {
            text: qsTr("Home")
        }
        TabButton {
            text: qsTr("Discover")
        }
        TabButton {
            text: qsTr("Activity")
        }
    }

    StackLayout {
        width: parent.width
        currentIndex: bar.currentIndex
        Item {
            id: homeTab
        }
        Item {
            id: discoverTab
        }
        Item {
            id: activityTab
        }
    }

    Column {

        x : 200
          Switch {
              text: qsTr("Wi-Fi")
              LayoutMirroring.enabled: true
              onPositionChanged: {
                  console.log(position)
              }
          }
          Switch {
              text: qsTr("Bluetooth")
          }
      }

    Column {
          RadioButton {
              checked: true
              text: qsTr("First")
          }
          RadioButton {
              text: qsTr("Second")
          }
          RadioButton {
              text: qsTr("Third")
          }
      }

    DelayButton{
        x : parent.width - 100
        y : parent.height - 50
        width : 100
        height: 50
        delay: 3000

        onProgressChanged: {
            console.log(progress)
        }
    }

}
