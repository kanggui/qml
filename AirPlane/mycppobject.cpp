#include "mycppobject.h"

void MyCppObject::resetAllValues()
{
    m_value1 = "";
    m_value2 = "";
    emit value1Changed(m_value1);
    emit value2Changed(m_value2);
}
void MyCppObject::setValue1(const QString& v)
{
    if (m_value1 != v)
    {
        m_value1 = v;
        emit value1Changed(m_value1);
    }
}
QString MyCppObject::value1() const { return m_value1; }
void MyCppObject::setValue2(const QString& v)
{
    if (m_value2 != v)
    {
        m_value2 = v;
        emit value2Changed(m_value2);
    }
}
QString MyCppObject::value2() const {return m_value2; }
