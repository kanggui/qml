#include "mytimer.h"

MyTimer::MyTimer(QObject * p):QObject(p)
{
    m_timer = new QTimer;
    connect(m_timer, &QTimer::timeout, this, &MyTimer::timeout);
    m_timer->setInterval(m_interval);
}

void MyTimer::start()
{
    m_timer->start();
}

void MyTimer::stop()
{
    m_timer->stop();
}
