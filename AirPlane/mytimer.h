#ifndef MYTIMER_H
#define MYTIMER_H

#include <QObject>
#include <QTimer>
#include <QDebug>

class MyTimer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int interval READ interval WRITE setInterval NOTIFY intervalChanged)
public:
    MyTimer(QObject * p = NULL);
    void setInterval(int msec){
        m_interval = msec;
        m_timer->setInterval(msec);
        emit intervalChanged();
    }
    int interval(){
        return m_interval;
    }

signals:
    void intervalChanged();
    void timeout();
    void activeChanged();

public slots:
    void start();
    void stop();
private:
    QTimer* m_timer;
    int m_interval = 1000;
};

#endif // MYTIMER_H
