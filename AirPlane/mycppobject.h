#ifndef MYCPPOBJECT_H
#define MYCPPOBJECT_H

#include <QObject>

class MyCppObject : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString value1 READ value1 WRITE setValue1 NOTIFY value1Changed )
    Q_PROPERTY(QString value2 READ value2 WRITE setValue2 NOTIFY value2Changed )
public:
    MyCppObject(const QString& value1, const QString& value2): QObject(), m_value1(value1), m_value2(value2) {  }
    Q_INVOKABLE void resetAllValues();

    void setValue1(const QString& v);

    QString value1() const;// { return m_value1; }
    void setValue2(const QString& v);

    QString value2() const;// {return m_value2; }
signals:
    void value1Changed(const QString&);
    void value2Changed(const QString&);
private:
    QString m_value1;
    QString m_value2;
};
#endif // MYCPPOBJECT_H
