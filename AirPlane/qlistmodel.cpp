#include "qlistmodel.h"

QListModel::QListModel(QObject *parent)
    : QAbstractListModel(parent)
{
    m_roleName.insert(animeNameRole, "animeName");
    m_roleName.insert(animeLeadRole, "animeLead");

    m_date = {
        { "vivo", "1200"},
        { "meizu", "1300"},
        { "apple", "1400"},
        { "huawei", "1500"},
    };

}

QHash<int, QByteArray> QListModel::roleNames() const
{
   return m_roleName;
}

int QListModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return m_date.size();
}

QVariant QListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    switch(role)
    {
        case animeNameRole:
            return m_date.value(index.row()).qsAnimeName;
        case animeLeadRole:
            return m_date.value(index.row()).qsAnimeLead;
        default:
            break;
    }

    return QVariant();
}

QAbstractItemModel *QListModel::model()
{
    return this;
}
