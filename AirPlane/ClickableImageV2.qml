import QtQuick 2.0
import QtQuick.Controls 2.3

Item {
    id: item
    width: container.childrenRect.width
    height: container.childrenRect.height
    property alias text: label.text
    property alias source: image.source
    signal clicked

    Column {
        id: container
        Image {
            id: image
            width: 50
            height: 50
        }
        Text {
            id: label
            width: image.width
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
            color: "red"

        }
    }
    MouseArea {
        anchors.fill: parent
        onClicked: item.clicked()
    }
}
