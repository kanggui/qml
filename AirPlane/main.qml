import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.14
import QtQuick.Controls 1.4
import CustomComponents 1.0
import WorkClass 1.0
import QtQuick.Shapes 1.14
import QtQuick.Controls 2.14
import QtQuick 2.14
import QtQuick.Controls.Styles 1.4

Window {
    id : root
    visible: true
    width: 680
    height: 480
    //flags:  Qt.FramelessWindowHint
    title: qsTr("TabBar")
   //color: "#000"


    Image {
        x: 40; y: 80
        width: 50
        height: 50
        source: "resources/1.jpeg"
        NumberAnimation on x {
            to: 600
            duration: 4000
            loops: Animation.Infinite
       }

        RotationAnimation on rotation {
            to: 360
            duration: 4000
            loops: Animation.Infinite
        }
    }

    ClickableImageV2 {
        id: rocket1
        x: 40; y: 200
        source: "resources/1.jpeg"
        text: "animation on property"
        NumberAnimation on y {
            to: 40; duration: 4000
        }
    }

    ClickableImageV2 {
        id: rocket2
        x: 152; y: 200
        source: "resources/1.jpeg"
        text: "behavior on property"
        Behavior on y {
            NumberAnimation { duration: 4000 }
        }
        onClicked: y = 40
    }

    ClickableImageV2 {
        id: rocket3
        x: 264; y: 200
        source: "resources/1.jpeg"
        // onClicked: anim.start()
        onClicked: anim.restart()
        text: "standalone animation"
        NumberAnimation {
            id: anim
            target: rocket3
            properties: "y"
            from: 205
            to: 40
            duration: 4000
        }
    }

}
