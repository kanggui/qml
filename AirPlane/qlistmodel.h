#ifndef QLISTMODEL_H
#define QLISTMODEL_H


#include <QAbstractListModel>
#include <QObject>

struct LIST_ITEM_INFO {
    QString qsAnimeName;
    QString qsAnimeLead;
};


class QListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit QListModel(QObject *parent = nullptr);

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QHash<int,QByteArray> roleNames() const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Q_INVOKABLE QAbstractItemModel* model();

public:
    enum LIST_ITEM_ROLE
    {
        animeNameRole = Qt::UserRole+1,
        animeLeadRole,
    };

private:
    QList<LIST_ITEM_INFO>           m_date;
    QHash<int,QByteArray>           m_roleName;

};


#endif // QLISTMODEL_H
