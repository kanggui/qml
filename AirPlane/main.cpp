#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include <QUrl>
#include <QDebug>
#include <QQmlContext>

#include "mycppobject.h"
#include "mytimer.h"
#include "cbusiness.h"
#include "qlistmodel.h"

int main(int argc, char *argv[])
{

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    //注册类
    //qmlRegisterType<PaintItem>("PaintItemModule",1,0,"PaintItem");

#if 1
    QQmlApplicationEngine engine;

    //MyCppObject cppobj("hello", "world");
    //engine.rootContext()->setContextProperty("cppobj", &cppobj);
    qmlRegisterType<MyTimer>("CustomComponents", 1, 0, "MyTimer");
    qmlRegisterType<CBusiness>("WorkClass", 1, 0, "CBusiness");

    QListModel listModel;
    engine.rootContext()->setContextProperty("QListModel", &listModel);

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);
#else
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QQuickView view;// load the qml file
    view.setSource(url);

    QObject *object = view.rootObject();
    if(object)
    {
        object->setProperty("width", 800);
        object->setProperty("height", 800);

        QObject *btn = object->findChild<QObject *>("btn");
        if(btn)
            btn->setProperty("color", "green");
        qDebug() << "ok";
    }
    else
    {
        qDebug() << "null";
    }
    view.show();

#endif
    return app.exec();

#if 0
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
#endif
}
