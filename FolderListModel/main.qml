import QtQuick 2.9
import QtQuick.Window 2.2
import Qt.labs.folderlistmodel 2.1

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")


    ListView {
        width: 200; height: 400

        FolderListModel {
            id: folderModel
            folder: "file:\\\C:\\Users\\Administrator\\Desktop\\qml\\"
            //showDirs: false
            showFiles: true
            showDirsFirst: true
            nameFilters: ["*"]

            Component.onCompleted: {
                console.log(folder)
            }
        }

        Component {
            id: fileDelegate
            Text {
                text: filePath + "/" + fileName + ":" + fileSize
                font.pixelSize: 30
            }
        }

        model: folderModel
        delegate: fileDelegate
    }
}
