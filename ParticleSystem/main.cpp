#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QDebug>
#include <QSslSocket>
#include <QThread>

extern "C"{
#include <stdio.h>
#include <string.h>
#include <curl/curl.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <ifaddrs.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>

#include "zlog.h"
}

//#include "test.pb.h"

using namespace std;

#define CURL_DEBUG 1
#define CURL_WAIT_TIMEOUT_MSECS 60000 //60s
#define CURL_MULIT_MAX_NUM 5
#define ETH_INTERFACE_STA       "ens33"//""

#define log_debug()             qDebug() <<  "[" << __FILE__ << "][" <<  __FUNCTION__ << "][" << __LINE__ << "]:"


static size_t recive_data_fun( void *ptr, size_t size, size_t nmemb, void *stream)
{
    log_debug() << (char *)ptr;
    //return fwrite(ptr,size,nmemb,(FILE*)stream);
    return size*nmemb;
}

static size_t read_head_fun( void *ptr, size_t size, size_t nmemb, void *stream)
{
    char head[2048] = {0};
    //memcpy(head,ptr,size*nmemb+1);
    log_debug() << (char *)ptr;
    return size*nmemb;
}


int GetMac(char *mac)
{
    struct ifreq ifr;
    int sd;

    if (mac == NULL)
    {
        printf("GetMac error p null\n");
        return -1;
    }

    bzero(&ifr, sizeof(struct ifreq));
    if( (sd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("get %s mac address socket creat error\n", ETH_INTERFACE_STA);
        return -1;
    }

    strncpy(ifr.ifr_name, ETH_INTERFACE_STA, sizeof(ifr.ifr_name) - 1);

    if(ioctl(sd, SIOCGIFHWADDR, &ifr) < 0)
    {
        printf("get %s mac address error\n", ETH_INTERFACE_STA);
        close(sd);
        return -1;
    }

    for(int i = 0; i < 6; i++)
    {
        mac[i] = ifr.ifr_hwaddr.sa_data[i];
    }

    close(sd);

    return 0;
}

//using namespace google::protobuf;



int main(int argc, char *argv[])
{ 

#if 0
    qDebug() << "hello";
    return 0;


    curl_global_init(CURL_GLOBAL_ALL);

    string deviceHash = "e54a270cc00b1e0862db2b2f87f34917";
    QMap<string, string> header_map;
    QMap<string, string> metaData_map;

    CURL *curl_handle = curl_easy_init();
    if(curl_handle)
    {
        printf("init ok\n");

        string url = "https://pan.baidu.com/rest/2.0/xpan/device?";
        metaData_map.insert("method", "register");
        metaData_map.insert("device_type", "202109221645318209");
        metaData_map.insert("device_addr", deviceHash);
        metaData_map.insert("app_id", "498065");

        header_map.insert("Content-Type", "application/x-www-form-urlencoded");
        header_map.insert("User-Agent", "pan.baidu.com");

        QMap<string, string>::const_iterator iterator = metaData_map.constBegin();
        while (iterator != metaData_map.constEnd())
        {
            url += iterator.key() + "=" + iterator.value() + "&";
            ++iterator;
        }

        struct curl_slist *headers = NULL;
        iterator = header_map.constBegin();
        while (iterator != header_map.constEnd())
        {
            headers = curl_slist_append(headers, (iterator.key() + ": " + iterator.value()).c_str());
            ++iterator;
        }

        curl_easy_setopt(curl_handle, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl_handle, CURLOPT_HTTPHEADER, headers);

        curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, NULL);//set download file
        curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, recive_data_fun);//set call back fun
        //curl_easy_setopt(curl_handle, CURLOPT_HEADERFUNCTION, read_head_fun);


#if 0//CURL_DEBUG
        curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1);
#endif

        CURLcode res = curl_easy_perform(curl_handle);
        log_debug() << "res : " << res;
        curl_slist_free_all(headers);

    }

    curl_easy_reset(curl_handle);

    {
        string url = "https://d.pcs.baidu.com/file/03453564egbd827e542e64fc187a9ba6?fid=2811138592-250528-141755130977743&rt=pr&sign=FDtAERV-DCb740ccc5511e5e8fedcff06b081203-Q%2BHRS6dJYsqrAfvcuMY%2BaAYlm70%3D&expires=8h&chkbd=0&chkv=3&dp-logid=2485144583127849199&dp-callid=0&dstime=1689750876&r=936667193&origin_appid=26195059&file_type=0&access_token=121.121db7b6988de6e29f2e3574b35b88b4.YCqKiVSTWTnLiL2vbYBVP-JpbE6brYMUGkaNEu5.-Sl9XA";

        curl_easy_setopt(curl_handle, CURLOPT_URL, url.c_str());
        //curl_easy_setopt(curl_handle, CURLOPT_HTTPHEADER, headers);

        curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, NULL);//set download file
        curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, recive_data_fun);//set call back fun


        CURLcode res = curl_easy_perform(curl_handle);
        log_debug() << "res : " << res;
        if(res == CURLE_OK)
        {

            long httpcode;

            curl_easy_getinfo(curl_handle, CURLINFO_RESPONSE_CODE, &httpcode);
            if (httpcode == 301 || httpcode == 302)
            {
                log_debug() << "httpcode : " << httpcode;
                char *redirect_url = NULL;
                //1表示重定向次数，最多允许一次重定向
                curl_easy_setopt(curl_handle, CURLOPT_FOLLOWLOCATION, 1);
                curl_easy_getinfo(curl_handle, CURLINFO_REDIRECT_URL, &redirect_url);
                if (redirect_url != nullptr)
                {
                    log_debug() << "url : " << redirect_url;
                    //curl_free(redirect_url);
                }
            }
        }
    }


    printf("hello\n");
    curl_easy_cleanup(curl_handle);

    return 0;

#endif






    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);













    return app.exec();
}
