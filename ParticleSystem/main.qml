import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 2.3
import QtGraphicalEffects 1.0
import QtQuick.Particles 2.0

import "./particles"

Window {
    id : id_root
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")


    color: "#1f1f1f"

    ParticleSystem {
        id: particleSystem
    }
/*
    Emitter {
        id: emitter
        anchors.fill: parent
        system: particleSystem
        emitRate: 10
        lifeSpan: 2000
        lifeSpanVariation: 500
        size: 64
        sizeVariation: 32
        Tracer {
            color: 'green'
        }
    }*/


    Emitter {
        id: emitter
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        width: 1; height: 1
        system: particleSystem
        lifeSpan: 6400
        lifeSpanVariation: 400
        size: 32

/*
        velocity: AngleDirection {
            angle: -45
            angleVariation: 0
            //angleVariation: 15
            magnitude: 100
            //magnitudeVariation: 50
        }

        acceleration: AngleDirection {
            angle: 90
            magnitude: 25
        }*/
/*
        velocity: PointDirection {
            x: 100
            y: 0
            xVariation: 0
            yVariation: 100/6
        }*/

        velocity: TargetDirection {
            targetX: 100
            targetY: 0
            targetVariation: 100/6
            magnitude: 100
        }
    }

    ImageParticle {
        source: "file:/home/gui/linux/Gui/QML_u/particles/assets/star.png"
        system: particleSystem
        color: '#FFD700'
        colorVariation: 0.2

        rotation: 15
        rotationVariation: 5
        rotationVelocity: 45
        rotationVelocityVariation: 15
        entryEffect: ImageParticle.Scale
    }
/*
    Age {
        anchors.horizontalCenter: parent.horizontalCenter
        width: 240; height: 300
        system: particleSystem
        advancePosition: true
        lifeLeft: 1200
        once: true
        Rectangle{
            anchors.fill: parent
            border.color: "green"
            opacity: 0.1
        }
    }*/
/*
    Attractor {
        anchors.horizontalCenter: parent.horizontalCenter
        width: 160; height: 200
        system: particleSystem
        pointX: 0
        pointY: 0
        strength: 1.0
        Rectangle{
            anchors.fill: parent
            border.color: "red"
            opacity: 0.1
        }
    }*/
/*
    Friction {
        anchors.horizontalCenter: parent.horizontalCenter
        width: 240; height: 200
        system: particleSystem
        factor : 0.8
        threshold: 25
        Rectangle{
            anchors.fill: parent
            border.color: "red"
            opacity: 0.1
        }
    }*/
/*
    Gravity {
        anchors.fill: parent
        system: particleSystem
        magnitude: 50
        angle: 90
        Rectangle{
            anchors.fill: parent
            border.color: "red"
            opacity: 0.1
        }
    }*/
/*
    Turbulence {
        anchors.fill: parent
        system: particleSystem
        strength: 100
        Rectangle{
            anchors.fill: parent
            border.color: "red"
            opacity: 0.1
        }
    }*/
    Wander {
        anchors.fill: parent
        system: particleSystem
        affectedParameter: Wander.Position
        pace: 200
        yVariance: 240
        Rectangle{
            anchors.fill: parent
            border.color: "red"
            opacity: 0.1
        }
    }

    /*
    ItemParticle {
        id: particle
        system: particleSystem
        delegate: itemDelegate
    }

    Component {
        id: itemDelegate
        Rectangle {
            id: container
            width: 32*Math.ceil(Math.random()*3); height: width
            color: Math.random()*5>5?'white':"red"
        }
    }*/

}
