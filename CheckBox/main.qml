import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Rectangle{
        x : 300
        y : 0
        width:100
        height: 400

        CheckBox {
              tristate: true
              checkState: allChildrenChecked ? Qt.Checked :
                             anyChildChecked ? Qt.PartiallyChecked : Qt.Unchecked

              nextCheckState: function() {
                  if (checkState === Qt.Checked)
                      return Qt.Unchecked
                  else
                      return Qt.Checked
              }
          }
    }


    Rectangle{
        x : 100
        y : 0
        width: 100
        height: 400

        Column {
              ButtonGroup {
                  id: childGroup
                  exclusive: false
                  checkState: parentBox.checkState
              }

              CheckBox {
                  id: parentBox
                  text: qsTr("Parent")
                  checkState: childGroup.checkState
              }

              CheckBox {
                  checked: true
                  text: qsTr("Child 1")
                  leftPadding: indicator.width
                  ButtonGroup.group: childGroup
              }

              CheckBox {
                  text: qsTr("Child 2")
                  leftPadding: indicator.width
                  ButtonGroup.group: childGroup
              }
          }

    }


    ButtonGroup{
        id : btnGroup
        exclusive: true
        //buttons: col.children
    }

    Column {
        id : col
          CheckBox {
              //checked: true

              //autoExclusive : true

             // tristate:true
              text: qsTr("First")
              ButtonGroup.group: btnGroup
              onCheckStateChanged: {
                  console.log(checkState)
              }
          }
          CheckBox {
              //autoExclusive : true
              //tristate:true
              ButtonGroup.group: btnGroup
              text: qsTr("Second")
          }
          CheckBox {
             // autoExclusive : true
              //tristate:true
             // checked: true
              ButtonGroup.group: btnGroup
              text: qsTr("Third")
          }
      }
}
