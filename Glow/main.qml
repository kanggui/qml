import QtQuick 2.9
import QtQuick.Window 2.2
import QtGraphicalEffects 1.12
import QtQuick.Particles 2.12
import QtQuick.Controls 2.5

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")
    color: "gray"

    BusyIndicator{
        width: 100
        height: 100
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottomMargin: 20
    }

    MouseArea{
        anchors.fill: parent
        id : moues
        hoverEnabled: true
    }

    ParticleSystem{
        id : pariticlesystem
    }

    Emitter{
        id : emiiter
        anchors.centerIn: parent
        x : 300
        width: 300
        height: 300
        system: pariticlesystem
        emitRate: 10 //数量
        lifeSpan: 1000 //生命周期
        lifeSpanVariation: 5000 //
        size: 15
        velocity: PointDirection{
            x : +45
        }
    }

    ImageParticle{
        id : img
        source: "1.png"
        system: pariticlesystem
        color: Qt.rgba(153/255, 217/255, 234/255, 1.0)
        colorVariation: 0.3
    }

    Attractor{
        anchors.fill: parent
        enabled: true
        system: pariticlesystem
        pointX: moues.mouseX
        pointY: moues.mouseY
        strength: +1000000
        affectedParameter: Attractor.Acceleration
        proportionalToDistance: Attractor.InverseQuadratic
    }


    Text {
        width: 100
        height: 100
        id: txt
        text: qsTr("text")
        font.pixelSize: 50
        color: "white"

    }

    Glow {
        id : glow
          anchors.fill: txt
          radius: 20
          samples: 17
          color: "white"
          source: txt
          SequentialAnimation{
              running: true
              loops:   Animation.Infinite
              NumberAnimation {
                  target: glow
                  property: "spread"
                  duration: 1000
                  to : 0
                  //easing.type: Easing.InOutQuad
              }
              NumberAnimation {
                  target: glow
                  property: "spread"
                  duration: 1000
                  to : 0.5
                  //easing.type: Easing.InOutQuad
              }
          }
      }

}
