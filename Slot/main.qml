import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.5

Window {
    visible: true
    width: 800
    height: 480
    title: qsTr("Hello World")
    id :root

    signal sigTest(string name)
    function func(name){
        console.log(name)
    }

    Button{
        width: 200
        height: 100
        onClicked: {
            sigTest("hello")
        }
    }

    Component.onCompleted: {
        sigTest.connect(func)
    }

    Connections{
        target: root
//        onSigTest:{
//            console.log(name + "11")
//        }
        function onSigTest(str){
            console.log("1111")
            console.log(str + " 22")
        }
    }

    Component{
        id : com
        Button{
            signal btnSig(string name)
            onClicked: {
                console.log("123")
                btnSig("aaaaa")
            }
        }
    }

    Rect{
        com1: com
        com2: com
    }
}
