import QtQuick 2.0

Rectangle {
    width: 400
    height: 300
    property Component com1
    property Component com2

    border.color: "red"
    Loader{
        id : loader1
        sourceComponent: com1
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        anchors.right: parent.right
        anchors.rightMargin: 20

        Connections{
            target: loader1.item
            ignoreUnknownSignals: true
            onBtnSig:{
                console.log(name + "right")
            }
        }
    }

    Loader{
        id : loader2
        sourceComponent: com2
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        anchors.right: parent.right
        anchors.rightMargin: 150

        Connections{
            target: loader2.item
            onBtnSig:{
                console.log(name + "left")
            }
        }
    }
}
