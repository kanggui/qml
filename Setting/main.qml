import QtQuick 2.9
import QtQuick.Window 2.2
import Qt.labs.settings 1.0
import QtQuick.Controls 2.4

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Settings{
        id : setting
        property int num: 0
        category : "111"
        //fileName:"C:\\Users\\Administrator\\Desktop\\qml\\setting.ini"
    }
    Settings{
        id : setting2
        property int num: 0
        category : "222"
        //fileName:"C:\\Users\\Administrator\\Desktop\\qml\\setting.ini"
    }

    Text {
        id: txt
        text: setting.num + "  " + setting2.num
        font.pixelSize: 40
    }
    Button{
        x : 100
        onClicked: {
            setting.num++
            setting2.num++
            setting.setValue("value", 100)
            console.log(setting.value("value", 10))
        }
    }
}
