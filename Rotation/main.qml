import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    visible: true
    width: 800
    height: 480
    title: qsTr("Hello World")

    Rectangle {
        x : 650
          id: banner
          width: 150; height: 100; border.color: "black"

          Column {
              anchors.centerIn: parent
              Text {
                  id: code
                  text: "Code less."
                  opacity: 0.01
              }
              Text {
                  id: create
                  text: "Create more."
                  opacity: 0.01
              }
              Text {
                  id: deploy
                  text: "Deploy everywhere."
                  opacity: 0.01
              }
          }

          MouseArea {
              anchors.fill: parent
              onPressed: playbanner.start()
          }

          SequentialAnimation {
              id: playbanner
              running: false
              NumberAnimation { target: code; property: "opacity"; to: 1.0; duration: 2000}
              NumberAnimation { target: create; property: "opacity"; to: 1.0; duration: 2000}
              NumberAnimation { target: deploy; property: "opacity"; to: 1.0; duration: 2000}
          }
      }

    Rectangle {
        x : 400
        y : 300
         width: 75; height: 75; radius: width
         id: ball
         color: "salmon"

         MouseArea{
             anchors.fill: parent
             onClicked: {
                 ball.x += 50
                 ball.y += 50
             }
         }

         Behavior on x {
             NumberAnimation {
                 id: bouncebehavior
                 easing {
                     type: Easing.OutElastic
                     amplitude: 1.0
                     period: 0.5
                 }
             }
         }
         Behavior on y {
             animation: bouncebehavior
         }
         Behavior {
             ColorAnimation { target: ball; duration: 100 }
         }
     }

    Rectangle {
        x : 400
        y : 200
          width: 75; height: 75
          id: button
          state: "RELEASED"

          MouseArea {
              anchors.fill: parent
              onPressed: button.state = "PRESSED"
              onReleased: button.state = "RELEASED"
          }

          states: [
              State {
                  name: "PRESSED"
                  PropertyChanges { target: button; color: "lightblue"}
              },
              State {
                  name: "RELEASED"
                  PropertyChanges { target: button; color: "lightsteelblue"}
              }
          ]

          transitions: [
              Transition {
                  from: "PRESSED"
                  to: "RELEASED"
                  ColorAnimation { target: button; duration: 100}
              },
              Transition {
                  from: "RELEASED"
                  to: "PRESSED"
                  ColorAnimation { target: button; duration: 100}
              }
          ]
      }

    Rectangle {
        x : 500
          width: 100; height: 100
          color: "red"

          SequentialAnimation on color {
              ColorAnimation { to: "yellow"; duration: 1000 }
              ColorAnimation { to: "blue"; duration: 1000 }
          }
      }

    Rectangle {
          id: rect1
          x : 300
          width: 100; height: 100
          color: "red"

          PropertyAnimation on x {
              to: 100
              duration: 1000
          }
          PropertyAnimation on y {
              to: 100
              duration: 1000
          }

          PropertyAnimation on width {
              to: 200
              duration: 2000
          }
      }

    Rectangle {
          id: flashingblob
          width: 75; height: 75
          color: "blue"
          opacity: 1.0

          MouseArea {
              anchors.fill: parent
              onClicked: {
                  console.log("flashingblob")
                  animateColor.start()
                  animateOpacity.start()
                  animateWidth.start()
              }
          }

          PropertyAnimation {
              id: animateColor;
              target: flashingblob;
              properties: "color";
              to: "green";
              duration: 5000
          }

          NumberAnimation {
              id: animateOpacity
              target: flashingblob
              properties: "opacity"
              from: 0.1
              to: 1.0
              duration: 5000
         }

          NumberAnimation {
              id: animateWidth
              target: flashingblob
              properties: "width"
              from: 75
              to: 200
              duration: 5000
         }
      }

    Rectangle{
        id : rect
        x : 100
        y : 200
        width: 200
        height: 200
        color: "red"
    }


/*
        states: [
            State {
                name: "normal"
                PropertyChanges {
                    target: rect
                    color: "green"
                }
            },
            State {
                name: "red_color"
                PropertyChanges {
                    target: rect
                    color: "red"
                }
            },
            State {
                name: "blue_color"
                PropertyChanges {
                    target: rect
                    color: "blue"
                }
            }
        ]

        border.width: 5
        border.color: "green"
        antialiasing: false
        radius: 20
        MouseArea{
            anchors.fill: parent
            onPressed:{
                rect.state = "red_color"
            }
            onReleased: {
                rect.state = "blue_color"
            }
        }

        Keys.onReturnPressed:
            console.log("return key press")
    }*/
}
