import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.5

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")




    ComboBox{
        //editable: true
        //textRole: "text"
        width: 200
        displayText: currentText + " hello"
        //valueRole : "name"
        model: [//ListModel {
            //id: model
              "Banana" ,
              "Apple" ,
              "Coconut" ,
        ]//}
        onAccepted: {
              if (find(editText) === -1)
                  model.append({text: editText})
          }
        onCurrentIndexChanged: {
            console.log(currentIndex)
        }
        onCurrentTextChanged: {
            console.log(currentText)
        }

    }

    ComboBox {
        x : 200
         currentIndex: 1
         displayText: "Size: " + currentText
         model: ["S", "M", "L"]
     }

    ComboBox {
        y: 200
          textRole: "key"
          model: ListModel {
              ListElement { key: "First"; value: 123 }
              ListElement { key: "Second"; value: 456 }
              ListElement { key: "Third"; value: 789 }
          }
      }

    ComboBox {
        x : 200
        y : 200
          model: 10
          editable: true
          validator: RegExpValidator {
              //top: 20
              //bottom: 0
              regExp:/[0-9A-Z]+[.][0-9]/
          }
          onAcceptableInputChanged: {
              console.log(acceptableInput)
          }
      }
}
